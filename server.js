var express = require('express'),
  app = express(),
  port = process.env.PORT || 9000;

var routes = require('./api/routes/dellrestRoutes'); //importing route
routes(app);

app.listen(port);

