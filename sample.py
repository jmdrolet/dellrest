from urllib2 import Request, urlopen, URLError
import json

request = Request('http://dellrest.azurewebsites.net/products/12603,11144')

# request = Request('http://www.dell.com/support/search/ca/en/cadhs1/product?appid=440CFE2D-3150-42D3-8236-0C1954EBF0D2&productids=406')

class CategoryDetails(object):
    def __init__(self, tagid, productname, friendlycategorypath):
    		self.tagid = tagid
		self.productname = productname
		self.friendlycategorypath = friendlycategorypath

try:
	response = urlopen(request)
	products = json.loads(response.read())
	product_categories = []
	if (products['CategoryDetails']):
		for x in products['CategoryDetails']:
				if 'ProductName' in x and 'FriendlyCategoryPath' in x:
					product_categories.append(x['FriendlyCategoryPath'])
		print ";".join(product_categories)
except URLError, e:
    print 'Got an error code:', e