module.exports = function(app) {
  var dellrest = require('../controllers/dellrestController.js');

  app.route('/products/:tagid')
    .get(dellrest.get_product);

};