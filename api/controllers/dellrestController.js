var data = require('../data/data.json');
const _ = require('underscore');

exports.get_product = function(req, res) {
    console.log(req.params.tagid)
    var tagIds = req.params.tagid.split(',');
    var tag = _.filter(data.CategoryDetails, function(tag){ 
        return tagIds.indexOf(tag.TagId) > -1; 
    });

    if (tag == undefined) { tag = {}};
    res.setHeader('Content-Type', 'application/json');
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.send('{"CategoryDetails":'+JSON.stringify(tag)+'}');
};
